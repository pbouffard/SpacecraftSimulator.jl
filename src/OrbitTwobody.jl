module OrbitTwoBody

using Dates
using StaticArrays

using SatelliteDynamics: R_EARTH, Epoch, sOSCtoCART, EarthInertialState, sim!, orbit_period
using SatelliteToolbox: init_orbit_propagator, init_space_indices, DatetoJD, propagate!, period

function keplerian(epoch0_datetime::DateTime, osculating_oe0; library=:SatelliteDynamics, tincr_s=5*60)
  ret = nothing
  if library == :SatelliteDynamics
    # https://sisl.github.io/SatelliteDynamics.jl/latest/tutorials/orbit_propagation_example/
    # Declare simulation initial Epoch
    epc0 = Epoch(year(epoch0_datetime), month(epoch0_datetime), day(epoch0_datetime), hour(epoch0_datetime), minute(epoch0_datetime), second(epoch0_datetime), millisecond(epoch0_datetime)*1e6) 
    
    # Declare initial state in terms of osculating orbital elements
    oe0 = [
    osculating_oe0.a_m, # a, Semi-major axis [m]
    osculating_oe0.e, # Eccentricity [dimensionless]
    osculating_oe0.i_deg, # Inclination [deg]
    osculating_oe0.Ω_deg, # Right Ascension of the Ascending Node (RAAN) [deg]
    osculating_oe0.ω_deg, # Argument of Perigee [deg]
    osculating_oe0.M_deg, # Mean anomaly [deg]
    ]
    
    # Convert osculating elements to Cartesean state
    eci0 = sOSCtoCART(oe0, use_degrees=true)
    
    # Set the propagation end time to one orbit period after the start
    T = orbit_period(oe0[1])
    @show T
    epcf = epc0 + 0.75*T
    
    # Initialize State Vector
    orb = EarthInertialState(epc0, eci0, dt=1.0,
    mass=1.0, n_grav=0, m_grav=0,
    drag=false, srp=false,
    moon=false, sun=false,
    relativity=false
    )
    
    orbfull = EarthInertialState(epc0, eci0, dt=1.0,
    mass=100.0, n_grav=20, m_grav=20,
    drag=true, srp=true,
    moon=true, sun=true,
    relativity=true
    )
    
    # Propagate the orbit
    t, epc, eci = sim!(orbfull, epcf)
    r = SVector{3}.(c for c in eachcol(eci[1:3,:]))
    v = SVector{3}.(c for c in eachcol(eci[4:6,:]))
    global ret = (;t, r, v)
    # @info "ret=$ret"
  elseif library == :SatelliteToolbox
    epc0 = DatetoJD(epoch0_datetime)
    T = period(osculating_oe0.a_m, osculating_oe0.e, osculating_oe0.i_deg, :J0)
    @show T
    t = 0:tincr_s:T
    orbp = init_orbit_propagator(Val(:twobody), epc0, osculating_oe0.a_m, osculating_oe0.e, deg2rad(osculating_oe0.i_deg), deg2rad(osculating_oe0.Ω_deg), deg2rad(osculating_oe0.ω_deg), deg2rad(osculating_oe0.f_deg))
    o, r, v = propagate!(orbp, t)
    ret = (;t, r, v)
  else
    throw(ErrorException("not implemented"))
  end
  return ret
end


end