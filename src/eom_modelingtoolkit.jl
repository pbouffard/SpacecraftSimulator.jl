module EquationsOfMotionModelingToolkit

using ModelingToolkit
using ModelingToolkit
# using OrdinaryDiffEq
using DifferentialEquations
using LinearAlgebra
using SpacecraftSimulator
using Parameters
using PrettyPrint

# using ordinary
# using Rotations
# using Rotations: kinematics, skew

function kinematics(σ, ω, J)
  0.5*(J + skew(σ) + σ*σ')*ω
end

function skew(v)
  [0   -v[3]  v[2];
  v[3] 0    -v[1];
  -v[2] v[1]  0]
end

function rotationaleom(; name)
  @parameters t J[1:3,1:3] Lc[1:3]
  @variables σ[1:3](t) ω[1:3](t) #x(t) # σ[1:3](t) ω[1:3](t) #u[1:3](t)
  @show σ, ω
  D = Differential(t)
  
  eqs = [
  D.(σ) .~ kinematics(σ, ω, J)
  D.(ω) .~ inv(J) * (-skew(ω) * J * ω + Lc);
  ]
  
  sys = ODESystem(eqs, name=name)
  vs = (; σ, ω)
  ps = (; J, Lc)
  (; sys, vs, ps) # J[1:3,1:3])
end
export rotationaleom

function mrp_shadowing_callback(mrpindices)
  function shadowcondition(u, t, intgr)
    cond = norm(u[mrpindices]) - 1.0
    @info "shadowcondition (u = $(u), t = $t, cond = $cond"
    cond
  end
  
  function shadow!(intgr)
    @info "shadow! (u = $(intgr.u)) t = $(intgr.t)"
    intgr.u[mrpindices] = shadowmrp(intgr.u[mrpindices])
    @info "intgr.u[$mrpindices] = $(intgr.u[mrpindices])"
  end
  ContinuousCallback(shadowcondition, shadow!, affect_neg! = nothing)
end

function testrotationaleom()
  @named eom = rotationaleom()
  @unpack σ, ω = eom.vs
  @unpack J, Lc = eom.ps
  @show σ
  x0 = [
  σ[1:3] .=> zeros(3);
  ω[1:3] .=> (0.1, 0.0, 0.0)
  ]
  @info "Initial conditions\n$(pformat(x0))"
  
  p = [
  (J .=> diagm([1.0, 2, 3]))...,
  (Lc .=> [0.0, 0.0, 0.0])..., 
  ]
  @info "Parameters\n$(pformat(p))"
  
  tspan = (0.0, 20.0)
  
  
  prob = ODEProblem(eom.sys, x0, tspan, p, callback=mrp_shadowing_callback(1:3))
  solve(prob, Tsit5())
end
export testrotationaleom

function txneom_array()
  @parameters t m
  @variables x[1:3](t) v[1:3](t)
  # @variables x[1](t) v[1](t)
  D = Differential(t)
  @show x
  eqs = [D.(x) .~ v; D.(v) .~ 1.0/m] #.|> simplify
  @. eqs = [D(x) ~ v; D(v) ~ [1,1,1] / m]
  # eqs = [[D(x[i]) ~ v[i] + x[3] + x[2] for i in 1:3]; [D(v[i]) ~ 1.0 for i in 1:3]]
  # eqs = [
  #   D(x[1]) ~ x[1]
  #   D(v[1]) ~ 1
  #   ]
  @show eqs
  @named sys = ODESystem(eqs; default_u0 = [x => zeros(3), v => zeros(3)], default_p = [m => 1.0])
end
export txneom_array

function translationaleom(name)
  @parameters t m Fx Fy Fz
  @variables x(t) y(t) z(t) vx(t) vy(t) vz(t) w(t)
  # @show x
  # @show v
  D = Differential(t)
  
  eqs = [
  D(x) ~ vx
  D(y) ~ vy
  D(z) ~ vz
  D(vx) ~ 1/m * Fx + w
  D(vy) ~ 1/m * Fy
  D(vz) ~ 1/m * Fz
  ]
  sys = ODESystem(eqs, name=name)
  # vs = (; x, v)
  # ps = (; m, F)
  # (; sys, vs, ps)
end

function simpledecay(name)
  @variables x
  @parameters t
  
  D = Differential(t)
  eqs = [
  D(x) ~ -x
  ]
  
  sys = ODESystem(eqs, name=name)
end

function testtranslationaleom()
  
  teom1 = translationaleom(:teom1)
  # teom2 = translationaleom(:teom2)
  decay = simpledecay(:decay)
  # @unpack x, v = eom.vs
  # @unpack m, F = eom.ps
  # @parameters t m Fx Fy Fz
  # @variables x(t) y(t) z(t) vx(t) vy(t) vz(t)
  @show teom1
  # @show teom2
  
  @parameters t
  # @variables dummy
  # @parameters dummyp
  # D = Differential(t)
  
  eqs = [teom1.w ~ decay.x]
  
  x0 = [
  (teom1.x, teom1.y, teom1.z) .=> zeros(3); 
  # (teom2.x, teom2.y, teom2.z) .=> zeros(3); 
  (teom1.vx, teom1.vy, teom1.vz) .=> zeros(3);
  # (teom2.vx, teom2.vy, teom2.vz) .=> zeros(3);
  (teom1.vx, teom1.vy, teom1.vz) .=> zeros(3);
  # (teom2.vx, teom2.vy, teom2.vz) .=> zeros(3);
  teom1.w => 0.0;
  decay.x => 1.0;
  # dummy => 0.0
  ]
  @info "Initial conditions\n$(pformat(x0))"
  
  p = [
  teom1.m => 1.0; (teom1.Fx, teom1.Fy, teom1.Fz) .=> [1.0, 0.0, 0.0];
  # teom2.m => 1.0; (teom2.Fx, teom2.Fy, teom2.Fz) .=> [1.0, 0.0, 0.0];
  # teom2.w => 0.0;
  # dummyp => 0.0
  ]
  @info "Parameters\n$(pformat(p))"
  
  tspan = (0.0, 10.0)
  sys = ODESystem(eqs, t, [], [], systems=[teom1, decay])
  prob = ODEProblem(sys, x0, tspan, p)
  sol = solve(prob)
end

function rigidbodyeom()
  teom = translationaleom()
  reom = rotationaleom()
  
  @parameters t
  # seems like there has to be at least one equation:
  @variables foo
  eqns = [0 ~ foo]
  
  sys = ODESystem(eqns, t, [foo], [], systems=[teom.sys, reom.sys], name=:rigidbody)
  (; sys, eom=(; teom, reom), foo)
end

function testrigidbodyeom()
  rigid = rigidbodyeom()
  @unpack x, v = rigid.eom.teom.vs
  @unpack σ, ω = rigid.eom.reom.vs
  
  @unpack m, F = rigid.eom.teom.ps
  @unpack J, Lc = rigid.eom.reom.ps
  
  @unpack foo = rigid
  @show foo
  
  x0 = [
  getproperty(rigid.eom.teom.sys, :x₁) => 0.0;
  getproperty(rigid.eom.teom.sys, :x₂) => 0.0;
  getproperty(rigid.eom.teom.sys, :x₃) => 0.0;
  getproperty(rigid.eom.teom.sys, :v₁) => 0.0;
  getproperty(rigid.eom.teom.sys, :v₂) => 0.0;
  getproperty(rigid.eom.teom.sys, :v₃) => 0.0;
  getproperty(rigid.eom.reom.sys, :σ₁) => 0.0;
  getproperty(rigid.eom.reom.sys, :σ₂) => 0.0;
  getproperty(rigid.eom.reom.sys, :σ₃) => 0.0;
  getproperty(rigid.eom.reom.sys, :ω₁) => 0.0;
  getproperty(rigid.eom.reom.sys, :ω₂) => 0.0;
  getproperty(rigid.eom.reom.sys, :ω₃) => 0.0;
  foo => 0.0;
  # x[1:3] .=> zeros(3); 
  # v[1:3] .=> zeros(3);
  # σ[1:3] .=> zeros(3);
  # ω[1:3] .=> (0.1, 0.0, 0.0);
  ]
  @info "Initial conditions\n$(pformat(x0))"
  
  p = [
  getproperty(rigid.eom.teom.sys, :m) => 1.0; 
  getproperty(rigid.eom.teom.sys, :F₁) => 1.0;
  getproperty(rigid.eom.teom.sys, :F₂) => 1.0;
  getproperty(rigid.eom.teom.sys, :F₃) => 1.0;
  getproperty(rigid.eom.reom.sys, :J₁ˏ₁) => 1.0;
  getproperty(rigid.eom.reom.sys, :J₁ˏ₂) => 1.0;
  getproperty(rigid.eom.reom.sys, :J₁ˏ₃) => 1.0;
  getproperty(rigid.eom.reom.sys, :J₂ˏ₁) => 1.0;
  getproperty(rigid.eom.reom.sys, :J₂ˏ₂) => 1.0;
  getproperty(rigid.eom.reom.sys, :J₂ˏ₃) => 1.0;
  getproperty(rigid.eom.reom.sys, :J₃ˏ₁) => 1.0;
  getproperty(rigid.eom.reom.sys, :J₃ˏ₂) => 1.0;
  getproperty(rigid.eom.reom.sys, :J₃ˏ₃) => 1.0;
  getproperty(rigid.eom.reom.sys, :Lc₁) => 1.0;
  getproperty(rigid.eom.reom.sys, :Lc₂) => 1.0;
  getproperty(rigid.eom.reom.sys, :Lc₃) => 1.0;
  # (J .=> diagm([1.0, 2, 3]))...;
  # (Lc .=> [0.0, 0.01, 0.0])...; 
  ]
  @info "Parameters\n$(pformat(p))"
  
  tspan = (0.0, 10.0)
  
  prob = ODEProblem(rigid.sys, x0, tspan, p)
  # sol = solve(prob) #, Tsit5())
end

function trivialsystem()
  @parameters t
  @variables x(t)
  f(x) = 0 #[1 2 3; 4 5 6; 7 8 9] * x'x * x
  D = Differential(t)
  # eqs = [D(x) ~ f(x)]
  # eqs = [D(x[i]) ~ xi for (i, xi) in enumerate(f(x))]
  eqs = [D(x) ~ f(x)]
  # eqs = [
  #   D(x[1]) ~ x[1], 
  #   D(x[2]) ~ x[2], 
  #   D(x[3]) ~ x[3], 
  #   D(y) ~ 1.0]
  
  
  
  @show eqs
  sys = ODESystem(eqs, name=:trivial)
end

function rotationalsystem()
  @parameters t
  @variables foo
  eom = rotationaleom()
  triv = trivialsystem()
  # x0 = [rotationaleom.σ => 0.0, rotationaleom.ω => 1.0, rotationaleom.x => 0.0]
  # p = [rotationaleom.J => 1.0]
  connections = [0 ~ triv.x]
  connected = ODESystem(connections, t, [foo], [], systems=[eom, triv], name=:bar)
  x0 = [foo => 0.0]
  (; sys=connected, eom, triv, x0)
end

function rotationalprob()
  rotsys, eom, triv, xx0 = rotationalsystem()
  # @variables foo
  x0 = [[eom.σ => zeros(3), eom.ω => 1.0, eom.x => 0.0, triv.x => 0.0]; xx0]
  p = [eom.J => 1.0]
  tspan = (0.0, 1.0)
  @show x0
  @show p
  prob = ODEProblem(rotsys, x0, tspan, p)
end

function basiceom()
  @parameters m t
  @variables v(t) x(t)
  D = Differential(t)
  
  eqs = [
  D(v) ~ 1.0/m,
  D(x) ~ v,
  ]
  
  @show eqs
  return ODESystem(eqs, name=:simple)
end

function basicprob()
  eom = basiceom()
  tspan = (0.0, 10.0)
  u0 = [eom.x => 0.0, eom.v => 1.0]
  p = [eom.m => 1.0]
  prob = ODEProblem(eom, u0, tspan, p)
end

function lorenz()
  @parameters t σ ρ β
  @variables x(t) y(t) z(t)
  D = Differential(t)
  
  eqs = [D(x) ~ σ*(y-x),
  D(y) ~ x*(ρ-z)-y,
  D(z) ~ x*y - β*z]
  
  # u0 = [x => 1.0, y => 0.0, z => 0.0]
  # p = [σ => 10.0, ρ => 28.0, β => 8/3]
  sys = ODESystem(eqs,name=:lorenzsystem)
  # (; sys, u0, p)
end

function lorenzsys()
  lorenz1 = lorenz()
  lorenz2 = lorenz()
  @variables a
  @parameters γ t
  connections = [0 ~ lorenz1.x + lorenz2.y + a*γ]
  connected = ODESystem(connections,t,[a],[γ],systems=[lorenz1,lorenz2], name=:lorenzsys)
  (;eom=connected, lorenz1, lorenz2)
end

function lorenzprob()
  eom, lorenz1, lorenz2 = lorenzsys()
  tspan = (0.0, 10.0)
  # @parameters t σ ρ β
  # @variables x(t) y(t) z(t)
  @variables a
  @parameters γ
  
  u0 = [lorenz1.x => 1.0,
  lorenz1.y => 0.0,
  lorenz1.z => 0.0,
  lorenz2.x => 0.0,
  lorenz2.y => 1.0,
  lorenz2.z => 0.0,
  a => 2.0]
  
  @show u0
  
  p  = [lorenz1.σ => 10.0,
  lorenz1.ρ => 28.0,
  lorenz1.β => 8/3,
  lorenz2.σ => 10.0,
  lorenz2.ρ => 28.0,
  lorenz2.β => 8/3,
  γ => 2.0]
  
  @show p
  prob = ODEProblem(eom, u0, tspan, p)
end

function solvelorenz()
  prob = lorenzprob()
  sol = solve(prob, Rodas5())
end

end


# function continuous_eom(x, p::EOMParams, t)::SVector
#   # @show t, typeof(t)
#   # @show typeof(x)
#   # @show typeof(x) <: StateVector
#   # posn = pos(x) #x[1:3]
#   # TODO: why can't I use my pos() and vel() functions in types.jl?
#   vel = x[4:6]
#   u = p.control(x, t)
#   acc = 1/p.body.m * u[1:3]

#   σ = MRP(x[7:9]...)
#   ω = x[10:12]

#   σ̇ = kinematics(σ, ω)
#   ω̇ = inv(p.body.J) * (-skew(ω) * p.body.J * ω + u[4:6])
#   # @infiltrate
#   xdot = SVector{12}([vel; acc; σ̇; ω̇])
#   # @show x
#   # @show xdot
#   return xdot