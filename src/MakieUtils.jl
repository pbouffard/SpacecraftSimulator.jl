module MakieUtils

using GLMakie
using Observables

export newfigure2d, newfigure3d, setwindowtitle, setup_inspector, setup_shortcuts

# Makie examples: https://lazarusa.github.io/Webpage/codeJuliaMakie.html

function newfigure3d(title="New 3D Figure"; axmax=100.0)
  fig = Figure(resolution = (400, 1000))
  # zlims!()
  ax = fig[1,1]
  lscene = newlscene3d!(ax; axmax)
  set_window_config!(; title=title)
  display(fig)
  return (;fig, lscene)
end

function newlscene3d!(ax; axmax=100.0)
  lscene = LScene(ax, scenekw=(raw=false, limits=FRect3D(Vec3f0(-axmax), Vec3f0(axmax))))
  cam3d_cad!(lscene.scene; pan_button=Keyboard.left_control)
  scatter!(lscene, [0, 0, 1], [1, 1, 1]; show_axis=false)
  return lscene
end


function newfigure2d(title="New Figure"; xlabel="t", ylabel="y")
  fig = Figure(resolution = (1200, 700))
  ax = newaxis2d!(fig[1,1], title; xlabel, ylabel)
  display(fig)
  (;fig, ax)
end

function newaxis2d!(axin, title; xlims=(0, 100), ylims=(0, 10), xlabel, ylabel)
  ax = Axis(axin, title=title; xlabel, ylabel)
  # ax.aspect = DataAspect()
  set_window_config!(; title=title)
  # display(fig)
  xlims!(xlims...)
  ylims!(ylims...)
  hlines!(ax, 0; linewidth=2)
  vlines!(ax, 0; linewidth=2)
  return ax
end

function setwindowtitle(fig::AbstractPlotting.Figure, title)
  glscreen = fig.scene.current_screens[1].glscreen
  GLFW.SetWindowTitle(glscreen, title)
end

function setup_inspector(fig::AbstractPlotting.Figure, ax::Axis, p)
  # t = text!(ax, " ", position = first(p[1][]), visible = false, halign = :left)
  ploc = Node(Point2f0(0))
  hlinepts = lift(p -> (;xs = [0, p[1]], ys=[p[2], p[2]]), ploc)
  vlinepts = lift(p -> (;xs = [p[1], p[1]], ys=[0, p[2]]), ploc)
  t = text!(ax, lift(ploc -> string(ploc), ploc), position=ploc, visible = false, halign = :left)
  horiz_line = lines!(ax, lift(h->h.xs, hlinepts), lift(h->h.ys, hlinepts), visible = false, color=:blue)
  vert_line = lines!(ax, lift(h->h.xs, vlinepts), lift(h->h.ys, vlinepts), visible = false, color=:blue)
  # @show p
  on(events(fig.scene).mouseposition) do mp
    plt, idx = mouse_selection(fig.scene)
    # @show plt, idx
    if plt == p && idx != nothing
        pos = p[1][][idx]
        # @show pos
        # t.position = pos
        ploc[] = Point2f0(pos)
        # @show ploc, plocx, plocy
        # t[1] = string(pos)
        horiz_line.visible = vert_line.visible = t.visible = true
    else
      horiz_line.visible = vert_line.visible = t.visible = false
    end
  end
  ploc, t
end


function setup_simple_inspector(fig::AbstractPlotting.Figure, ax::Axis, p)
  on(events(fig.scene).mouseposition) do mp
    plt, idx = mouse_selection(fig.scene)
    # @show plt, idx
  end
end

function combo_figure(; num_2d_axes=2, resolution=(1200, 2400), xlabels=repeat(["t"], num_2d_axes), ylabels=repeat(["y"], num_2d_axes), axmax=100.0)
  fig = Figure(;resolution)
  # zlims!()
  # set_window_config!(; title=title)
  ax3d = fig[1,1]
  lscene = newlscene3d!(ax3d; axmax)
  axs = []
  for (i, (xlabel, ylabel)) in enumerate(zip(xlabels, ylabels))
    axin = fig[i+1,1]
    ax = newaxis2d!(axin, "Axis $i"; xlabel, ylabel)
    push!(axs, ax)
  end
  linkxaxes!(axs...)
  rowsize!(fig.layout, 1, Relative(1/2))
  tfinal = Node(1000.0)
  time_slider = IntervalSlider(fig[end+1, 1], range = LinRange(0, 1, 1000),
    startvalues = (0.2, 0.8))
  display(fig)
  return (;fig, ax3d, lscene, axs, time_slider)
end

function planet!(scene, radius; color=RGBAf0(0, 1, 1, 0.2))
  # mesh = mesh!(scene, Sphere(Point3f0(0, 0, 0), radius); color, show_axis=false)
  rotaxis = lines!(scene, [0, 0], [0, 0], radius*1.5*[-1, 1]; color=:purple, linewidth=2)
  sm = spheremesh!(scene, radius; color)
  return (; spheremesh=sm)
end

function point_at_origin(scene, eyeposition=nothing)
  ep = isnothing(eyeposition) ? scene.camera.eyeposition[] : eyeposition
  update_cam!(scene, ep, Vec3f0(0))
end

function setup_shortcuts(scene)
  on(scene.events.keyboardbuttons) do button
    # ispressed(button, Keyboard.left) && move_left()
    # ispressed(button, Keyboard.up) && move_up()
    # ispressed(button, Keyboard.right) && move_right()
    # ispressed(button, Keyboard.down) && move_down()
    ispressed(button, Keyboard.space) && point_at_origin(scene)
  end
end

function spheremesh!(scene, radius; color=RGBA(0, 1, 1, 0.2), origin=Point3f0(0), nsurf=24, nwire=9)

  function xyz(radius, n, nv)
    u = range(0,stop=2*π,length=n);
    v = range(0,stop=π,length=nv);
    
    x = zeros(n,nv); y = zeros(n,nv); z = zeros(n,nv)
    for i in 1:n
        for j in 1:nv
            x[i,j] = origin[1] + radius * cos.(u[i]) * sin(v[j]);
            y[i,j] = origin[2] + radius * sin.(u[i]) * sin(v[j]);
            z[i,j] = origin[3] + radius * cos(v[j]);
        end
    end
    return x, y, z
  end
  x, y, z = xyz(radius, nsurf, nsurf)
  surf = surface!(scene, x, y, z, show_axis=false, color=fill(color, nsurf, nsurf))
  # super hacky!
  x, y, z = xyz(radius*1.02, nsurf, nwire)
  wire = wireframe!(scene, x, y, z; show_axis=false, color=:gray, linewidth=2)
  # return (;x, y, z, n)
  (; surf, wire)
end

function test()
  # newfigure2d()
  scatter(rand(300))
  # newfigure3d()
  scatter(rand(300,3))
end

end