module SpacecraftSimulator

using StaticArrays
using Rotations
using Parameters
# using SpacecraftKinematics.Rotations

include("utils.jl")
include("types.jl")
include("eom.jl")
include("plots.jl")
include("OrbitTwobody.jl")
include("MakieUtils.jl")
include("Constants.jl")

end # module
