module Utils

using Rotations
# using DynamicalSystems
using JSON3
import SatelliteToolbox

M_to_f = SatelliteToolbox.M_to_f

"Return [0,0,0] no matter what arguments are given"
zeros3 = (x...;y...) -> zeros(3)

"Shadow MRP"
shadowmrp(σ::MRP) = MRP(shadow(params(σ))...)
export shadowmrp

function shadowmrp(σ; tol=1e-3)
  normsq = σ'σ
  @show normsq, typeof(normsq)
  normsq > (1 - tol) ? -σ/normsq : σ
end


# function savetrajectory(tvec, dset::Dataset)
#   open("test.json", "w") do f
#     JSON3.write(f, bodystates(tvec, dset))
#     println(f)
#   end 
# end
# export savetrajectory

# function loadtrajectory(filename)
#   return open(filename, "r") do f
#     JSON3.read(f, Vector{BodyState})
#   end
# end
# export loadtrajectory


indented(x; level=0) = replace(sprint(show, MIME("text/plain"), x), r"^"m => " "^level)
export indented

function test()
  #TODO
end

end