module Lorenz

using ModelingToolkit, OrdinaryDiffEq
using Plots

function lorenz(name)
  @parameters t σ ρ β
  @variables x(t) y(t) z(t)
  D = Differential(t)

  eqs = [D(x) ~ σ*(y-x),
        D(y) ~ x*(ρ-z)-y,
        D(z) ~ x*y - β*z]
  return ODESystem(eqs,name=name)
end


function connect()
  lorenza = lorenz(:lorenz1)
  lorenz2 = lorenz(:lorenz2)

  @variables a
  @parameters γ t
  connections = [0 ~ lorenza.x + lorenz2.y + a*γ]
  connected = ODESystem(connections,t,[a],[γ],systems=[lorenza,lorenz2])

  u0 = [lorenza.x => 1.0,
        lorenza.y => 0.0,
        lorenza.z => 0.0,
        lorenz2.x => 0.0,
        lorenz2.y => 1.0,
        lorenz2.z => 0.0,
        a => 2.0]

  p  = [lorenza.σ => 10.0,
        lorenza.ρ => 28.0,
        lorenza.β => 8/3,
        lorenz2.σ => 10.0,
        lorenz2.ρ => 28.0,
        lorenz2.β => 8/3,
        γ => 2.0]

  tspan = (0.0,100.0)
  prob = ODEProblem(connected,u0,tspan,p)
  sol = solve(prob,Rodas5())
  p = plot(sol, vars=(a, lorenza.x, lorenz2.z))
  (; sol, a, lorenza, lorenz2, p)
end

function docexample()
  @parameters t σ ρ β
  @variables x(t) y(t) z(t)
  D = Differential(t)
  
  eqs = [D(x) ~ σ*(y-x),
         D(y) ~ x*(ρ-z)-y,
         D(z) ~ x*y - β*z]
  
  lorenz1 = ODESystem(eqs,name=:lorenz1)
  lorenz2 = ODESystem(eqs,name=:lorenz2)
  
  @variables a
  @parameters γ
  connections = [0 ~ lorenz1.x + lorenz2.y + a*γ]
  connected = ODESystem(connections,t,[a],[γ],systems=[lorenz1,lorenz2])
  
  u0 = [lorenz1.x => 1.0,
        lorenz1.y => 0.0,
        lorenz1.z => 0.0,
        lorenz2.x => 0.0,
        lorenz2.y => 1.0,
        lorenz2.z => 0.0,
        a => 2.0]
  
  p  = [lorenz1.σ => 10.0,
        lorenz1.ρ => 28.0,
        lorenz1.β => 8/3,
        lorenz2.σ => 10.0,
        lorenz2.ρ => 28.0,
        lorenz2.β => 8/3,
        γ => 2.0]
  
  tspan = (0.0,100.0)
  prob = ODEProblem(connected,u0,tspan,p)
  sol = solve(prob,Rodas5())
  
  plot(sol,vars=(a,lorenz1.x,lorenz2.z))
end

# function plot(sol, ret)
# end

end