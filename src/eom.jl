export discrete_eom, continuous_eom
# using SpacecraftKinematics.Rotations
using Rotations
using Rotations: kinematics, skew
using LinearAlgebra
using SpacecraftSimulator
using SpacecraftSimulator: vel

function discrete_eom(x::SVector{6}, p::EOMParams, t)::SVector
  # @show t, typeof(t)
  pos = x[1:3]
  vel = x[4:6]
  # @infiltrate
  u = p.control(x, t)
  m = p.body.m
  xnext = SVector{6}(x + [vel*p.dt; 1/m * u[1:3]])
  # @show x
  # @show xnext
  return xnext
end

function continuous_eom(x, p::EOMParams, t)::SVector
  # @show t, typeof(t)
  # @show typeof(x)
  # @show typeof(x) <: StateVector
  # posn = pos(x) #x[1:3]
  # TODO: why can't I use my pos() and vel() functions in types.jl?
  vel = x[4:6]
  u = p.control(x, t)
  acc = 1/p.body.m * u[1:3]

  σ = MRP(x[7:9]...)
  ω = x[10:12]

  σ̇ = kinematics(σ, ω)
  ω̇ = inv(p.body.J) * (-skew(ω) * p.body.J * ω + u[4:6])
  # @infiltrate
  xdot = SVector{12}([vel; acc; σ̇; ω̇])
  # @show x
  # @show xdot
  return xdot
end