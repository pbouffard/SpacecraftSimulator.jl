using Rotations
using StructTypes

export BodyState, Body, EOMParams

const Vec3{T} = SVector{3,T}
const Vec3d = Vec3{Float64}
const MVec3d = MVector{3,Float64}
export Vec3d, MVec3d

"""
Contains the position and attitude and their rates. Units for position and velocity are up to the user. Attitude is using MRP parametrization, and attitude rate should be in rad/s but other units like deg/s could be used. The time field is up to the user to use/interpret as they see fit.

Frame of reference is unspecified and up to the user to manage.
"""
# TODO: try to get parametrization working here (i.e. BodyState{T})
@with_kw_noshow mutable struct BodyState
  time::Float64 = 0.0 # TODO: use specialized time type here?
  pos::Vec3d = zero(Vec3d) # body position
  vel::Vec3d= zero(Vec3d) # body velocity
  mrp::MRP = zero(MRP) # body attitude as MRPs
  angrate::Vec3d = zero(Vec3d) # body angular rate in rad/s
end

# Enables JSON3.write(bs)
StructTypes.StructType(::Type{BodyState}) = StructTypes.Struct()

"""
Construct vector of BodyStates from (N,) time vector and (N, 12) state vectors array
"""
function bodystates(tvec, statevectors)
  N = length(tvec)
  bs = Vector{BodyState}(undef, N)
  for i in 1:N
    bs[i] = BodyState(tvec[i], StateVec(statevectors[i, :]))
  end
  bs
end
export bodystates

@with_kw struct Body
  m::Float64 = 0.0  # mass in kg
  J = one(MMatrix{3,3})  # inertia tensor
end

@with_kw struct EOMParams
  body::Body = Body()
  control::Function = zeros3
  dt = 0.01 # timestep, seconds
end

# TODO: is this how to do these types?
const StateVector = StaticVector{12, Float64}
const StateVec = SVector{12, Float64}
const MStateVec = MVector{12, Float64} # mutable version
export StateVector, StateVec, MStateVec
pos(x::T) where T <: StateVector = x[1:3]
pos(x) = x[1:3]
vel(x::T) where T <: StateVector = x[4:6]
vel(x) = x[4:6]
mrp(x::T) where T <: StateVector = MRP(x[7:9]...)
mrp(x) = MRP(x[7:9]...)
angrate(x::T) where T <: StateVector = Vec3d(x[10:12])
angrate(x) = Vec3d(x[10:12])
export pos, vel, mrp, angrate

BodyState(x::StateVector) = BodyState(; pos=pos(x), vel=vel(x), mrp=mrp(x), angrate=angrate(x))
BodyState(t, x::StateVector) = BodyState(; time=t, pos=pos(x), vel=vel(x), mrp=mrp(x), angrate=angrate(x))
tostatevec(bs::BodyState) = StateVec([bs.pos; bs.vel; params(bs.mrp); bs.angrate])
export tostatevec

import Base.show
import Rotations: params
function Base.show(io::IO, ::MIME"text/plain", bs::BodyState)
  angle = rotation_angle(bs.mrp)
  print("""
  BodyState (t = $(bs.time))
  pos: $(bs.pos)
  vel: $(bs.vel)
  attitude:
   MRP: σ₁: $(bs.mrp.x) σ₂: $(bs.mrp.y) σ₃: $(bs.mrp.z)
   axis-angle: $(angle) rad = $(rad2deg(angle))° about $(rotation_axis(bs.mrp))
   DCM:
  """)
  println(join(split(indented(Matrix(bs.mrp); level=1), "\n")[2:end], "\n"))
  print("""
  angrate: $(bs.angrate)
  """)
  println()
end
