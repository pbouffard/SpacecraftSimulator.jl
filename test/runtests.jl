using Test
using SpacecraftSimulator
using LinearAlgebra
using PrettyPrinting
using Infiltrator
using StaticArrays
using DynamicalSystems
using Debugger

function runtests() 
  @testset "Demo Tests" begin
    @test true
    end

    @testset "Types" begin
      b = Body()
      @test b.m == 0
    @test b.J == I(3)

    b2 = Body(m=10)
    # @show b2

    bstate = BodyState()
    # pprint(bstate)
    @test bstate.pos == zeros(3)
  end

  @testset "EOM" begin
    @testset "discrete" begin
      params = EOMParams(
      body=Body(m=10.0),
      control=(x,t) -> SVector{6}([1.0, 0, 0, 0, 0, 0])
      )
      # @infiltrate
      x0 = zero(SVector{6})
      xdot = discrete_eom(x0, params, 0)
      @test xdot == [zeros(3); [0.1, 0, 0]]
      # pprint(xdot)
    end
    @testset "continuous" begin
      testcont()
      testcont2()
    end
  end
end

function testcont()
  params = EOMParams(
    body=Body(m=10.0),
    control=(x,t) -> SVector{6}([1.0, 0, 0, 0, 0, 0])
    )
  # @infiltrate
  x0 = zero(SVector{12})
  xdot = continuous_eom(x0, params, 0)
  @test xdot == [zeros(3); [0.1, 0, 0]; zeros(6)]
  # pprint(xdot)

  T = 10.0
  tvec = 0:params.dt:T
  ds = ContinuousDynamicalSystem(continuous_eom, x0, params)
  traj = trajectory(ds, T; dt=params.dt)
  # @infiltrate
  bs = BodyState(traj[end])
  @test bs.pos ≈ [5, 0, 0]
  @test bs.vel ≈ [1, 0, 0]
  @test bs.mrp == I(3)
  @test iszero(bs.angrate)
  (;traj, ds, tvec)
end


function testcont2()
  params = EOMParams(
    body=Body(m=10.0),
    control=(x,t) -> SVector{6}([1.0, 1.0, 0, 0, 0, 0])
    )
  # @infiltrate
  x0 = zero(SVector{12})
  xdot = continuous_eom(x0, params, 0)
  # @test xdot == [zeros(3); [0.1, 0, 0]; zeros(6)]
  # pprint(xdot)

  T = 10.0
  tvec = 0:params.dt:T
  ds = ContinuousDynamicalSystem(continuous_eom, x0, params)
  traj = trajectory(ds, T; dt=params.dt)
  # @infiltrate
  bs = BodyState(traj[end])
  # @test bs.pos ≈ [5, 0, 0]
  # @test bs.vel ≈ [1, 0, 0]
  # @test bs.mrp == I(3)
  # @test iszero(bs.angrate)
  (;traj, ds, tvec)
end
# traj = testcont()
# # # println(traj)
runtests()