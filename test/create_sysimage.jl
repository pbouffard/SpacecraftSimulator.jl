@info "Loading packages ..."
using GLMakie, Revise, Rotations
using PackageCompiler

@info "Creating sysimage ..."

PackageCompiler.create_sysimage(
    [:GLMakie, :Revise, :Rotations];
    sysimage_path="JuliaSysimage.dylib",
    precompile_execution_file=joinpath("test", "test_for_precompile.jl")
)