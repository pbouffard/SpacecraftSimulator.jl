Base.init_depot_path()
Base.init_load_path()

@show "hello"
# using LinearAlgebra
# using PrettyPrinting
# using Infiltrator
# using StaticArrays
# using DynamicalSystems
# using Debugger
# using GLMakie
# using AbstractPlotting
# using StaticArrays
# using Rotations
# using Parameters
# using SpacecraftSimulator

@eval Module() begin
    for (pkgid, mod) in Base.loaded_modules
      @info pkgid, mod
        if !(pkgid.name in ("Main", "Core", "Base"))
            eval(@__MODULE__, :(const $(Symbol(mod)) = $mod))
        end
    end
    for statement in readlines("precompile_trace.jl")
        try
            Base.include_string(@__MODULE__, statement)
        catch
            # See julia issue #28808
            @info "failed to compile statement: $statement"
        end
    end
end # module

empty!(LOAD_PATH)
empty!(DEPOT_PATH)