using Documenter
using SpacecraftSimulator

@info("Running `makedocs` with Documenter.")

makedocs(
  sitename="SpacecraftSimulator.jl",
  repo="https://gitlab.com/pbouffard/SpacecraftSimulator.jl/blob/{commit}{path}#L{line}",
  modules=[SpacecraftSimulator],
  authors="Patrick Bouffard <airpmb@fastmail.com> and contributors",
  sitename="SpacecraftSimulator.jl",
  format=Documenter.HTML(;
      prettyurls=get(ENV, "CI", "false") == "true",
      canonical="https://pbouffard.gitlab.io/SpacecraftSimulator.jl",
      assets=String[],
  ),
  pages=[
      "Home" => "index.md",
  ],
)