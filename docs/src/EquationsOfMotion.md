# Equations of Motion

EOM for the systems/components modeled should be compatible with [DifferentialEquations.jl](https://github.com/SciML/DifferentialEquations.jl). That is, they should have a type signature like (for [ODE](https://diffeq.sciml.ai/stable/types/ode_types/)'s which will be the most common types):

```
function f(x, p, t) -> dx # in-place aka iip
```
or
```
function f(dx, x, p, t) # in-place aka oop
```


This represents the $f$ in $\frac{dx}{dt} = f(x, p, t)$. Here,

* `dx` (output) is the time derivative of the state at time $t$
* `x` (input) is the state at time $t$
* `p` (input) are parameters
* `t` is the current time in seconds

The derivative `dx` should have the same 'geometry' as the state `x`, both of which are `AbstractArray`. In practice we will make liberal use of `ComponentArray`.

Parameters `p` 

**Notes**
1. DifferentialEquations.jl uses $u$ for states, but in our documentation we'll use the (arguably) more natural/conventional $x$, whereas $u$ will be reserved for controller outputs.
2. `ComponentArray <: DenseArray <: AbstractArray <: Any`
3. 

## Position

## Attitude

## Notes
1. `ComponentArray <: DenseArray <: AbstractArray <: Any`
