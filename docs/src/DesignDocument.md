# 6-DOF Spacecraft Simulator Design Documen

Design doc is based on this [gist](https://gist.github.com/mattBrzezinski/a87f61e6fe85ff0bcc7cdee9e5f943a3). Thanks [mattBrzezinski](https://github.com/mattBrzezinski)!

- [6-DOF Spacecraft Simulator Design Documen](#6-dof-spacecraft-simulator-design-documen)
  - [Contact Info](#contact-info)
  - [Executive Summary](#executive-summary)
  - [Tenets](#tenets)
  - [Measures of success](#measures-of-success)
  - [In Scope](#in-scope)
    - [Use cases](#use-cases)
    - [Models](#models)
    - [Software Engineering](#software-engineering)
      - [Workflows](#workflows)
  - [Out of Scope](#out-of-scope)
  - [Open issues](#open-issues)
  - [Use cases](#use-cases-1)
  - [Proposed solution summary](#proposed-solution-summary)
    - [Architecture](#architecture)
    - [Tools](#tools)
      - [Julia Packages](#julia-packages)
  - [Other solutions](#other-solutions)
  - [Dependencies](#dependencies)
  - [Functional requirements](#functional-requirements)
  - [Non-functional requirements](#non-functional-requirements)
  - [Metrics](#metrics)
  - [Alarms](#alarms)
  - [Appendix](#appendix)

## Contact Info
List of project members and their contact information.
* Patrick Bouffard

## Executive Summary
*A high-level paragraph explaining the purpose of the document.* 
*This should be written with the scope that anyone in the company can view this document and have an understanding of the proposal. Include a summary of the problem, any background information and the proposed solution.*

This doc describes the planned design and some implementation details for a 6-DOF simulator for motion of spacecraft, written in Julia.

The purpose of the Julia package is to organize the code. At this time there is no intent for this code to be used for other than purposes of learning by the original author, nor any intention to register the package in the general registry.

## Tenets
*Tenets are a list of guiding principles for your project.*
*Your tenets will keep you focused on your objective and keep you heading in the right direction.*

*Tenets are independent of each other, are clear and memorable.*

*They are used to define what a team cares more about.*

*Tenets are used to to help make difficult choices and trade offs in design decisions.*

1. Use existing Julia pacakges to the extent possible, with some caveats:
   - avoid use of unmaintained packages
1. For pedagogical purposes, in some cases implement functionality myself before using a package:
   * Look at existing packages first and ideally match their API to allow toggling between
   * Use packages to help with tests
1. While the package isn't intended for other than learning, that includes learning/practicing software engineering practices, so *try to make things production quality*, though as a secondary objective.


## Measures of success
*What will make this project successful?*

*This can be listed as a bullet point list of key features, workflows, and qualitative metrics; such as increasing performance by X%.*

*These measurements should not be considered pass/fail, but rather used in the retrospective to probe further questions into the system if needed.*

## In Scope
*A list of things that you will do / address / solve as a bullet point list.*

### Use cases
* Launch
  * Takeoff
  * Staging
  * Kick
  * Separation
* Planetary orbit/2 body
  * Earth
  * Moon
* Orbital maneuvers
  * pointing
* Orbital Transfer
* Interplanetary

### Models
* Newtonian dynamics - translational and rotational
* Non-interactive simulation
* MRP-based EOM
* Disturbances (external, internal)
  * Magnetic fields
  * Gravity gradient
  * Solar pressure
  * Atmospheric drag
  * Vibrations
  * Fuel slosh
* Actuators
  * RW
  * CMG
  * RCS thrusters
  * Main engines
  * Electric propulsion
* Sensors
  * Gyros
  * Accelerometers
  * Star trackers
* Controllers
  * include some basic ones
  * API for interfacing from elsewhere
* Subsystems (mostly in terms of metrics of their operation as function of pointing accuracy)
  * Comms

### Software Engineering
* Planning
  * Use GitLab issues
* Branching
  * master (main?) for releases
  * develop for merged functionality
* Minimal sanity testing
* Specific examples (though consider moving these to a separate package)
  * Drive most development through these
* Testing
* CI 📈
* Documentation
  * automatic building using Documentor.jl
  * output of examples included in documentation builds 📈
* data export
* GUI
  * plotting
  * parameter setting

#### Workflows
* GitLab
* Use `--local` for `dev`'d packages
  
📈 = Growth items

## Out of Scope
*A list of things that you will not do / no address / not solve as a bullet point list.*

*Sometimes it is easier to start with this before writing what is In Scope.*

* Visualization (SpacecraftVisualization.jl for this)

## Open issues
*List of issues that you currently do not know how to solve.*

*When having a design document review, these issues should be addressed and solutions proposed.*

(include link to GitLab issues)

## Use cases
*Create an architecture diagram of your proposed system including any external systems that it will interact with.*

*For more information on Use Cases visit the Wikipedia article.*

**TODO**: Diagram interactions with other pacakges

## Proposed solution summary
*Write about how your solution works in a detailed narrative, refer to appendices for diagrams, APIs, data structures, database schemas, etc.*

### Architecture
* Packaging
  * SpacecraftSimulator.jl initially catch-all for rapid development; split out as appropriate for functional separation of concerns and/or dependency management
* Modules
  * Utils
  * EOM
  * UI
  * Examples

### Tools
#### Julia Packages
(for the time being here's a list of potentially useful packages to use)
* DifferentialEquations.jl
* ComponentArrays.jl (or ModelingToolkit.jl?)
* Parameters.jl (`@with_kw`, `@unpack`)
* Rotations.jl
* RigidBodyDynamics.jl
* SatelliteToolbox.jl



## Other solutions
*Describe any alternative solutions that you may have prototyped.*

*Compare these alternative solutions to the proposed solution*

*List off the pros and cons of each solution, and why the proposed solution is the one you decided to go with.*

**TODO**

## Dependencies
*List any external dependencies that your solution has, and why.*

See [Project.toml]

## Functional requirements
A list of properties that can be expressed as “the system must do <requirement>”. 
The requirements should be measurable. 
Think of metrics.
E.g.:
* Users should be able to easily download prediction results.
* The system should have a logically defined API.
* The service should be able to handle 10,000 concurrent users.

## Non-functional requirements
A list of properties that can be expressed as “the system shall be <requirement>”.
E.g.:
* The system shall be secure
* Maintaining the system should be well defined and simple
* The service should be easily extendible, its components should be modular.

Some categories which non-functional requirements can be placed in are:
* Security
* Extendability / Maintenance
* Scalability
* Availability / Resilience
* Performance

## Metrics
Define metrics for both functional and non-functional properties of your solution.

## Alarms
Define your alarms based off of the metrics above.

## Appendix
Include things such as:
* Glossary
* FAQ
* One pagers
* API definitions
* Database Schemas
* Links to dependencies
* External documentation
