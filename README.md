# SpacecraftSimulator.jl

Personal project - implement a 6-DOF spacecraft simulator in Julia

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://pbouffard.gitlab.io/SpacecraftSimulator.jl/dev)
[![Build Status](https://gitlab.com/pbouffard/SpacecraftSimulator.jl/badges/master/pipeline.svg)](https://gitlab.com/pbouffard/SpacecraftSimulator.jl/pipelines)
[![Coverage](https://gitlab.com/pbouffard/SpacecraftSimulator.jl/badges/master/coverage.svg)](https://gitlab.com/pbouffard/SpacecraftSimulator.jl/commits/master)
