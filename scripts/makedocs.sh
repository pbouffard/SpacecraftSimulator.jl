#!/bin/bash

pushd docs
julia --project --color=yes make.jl
#open build/index.html
open "http://localhost:8000/"
popd
julia --project -e 'using LiveServer; serve(dir="docs/build")'