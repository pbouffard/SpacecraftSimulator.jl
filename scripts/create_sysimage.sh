#!/bin/bash -eu
# run from package folder
IMGEXT="dylib"
IMGFILE="JuliaSysimage.${IMGEXT}"
if test -f ${IMGFILE}; then
    cp ${IMGFILE} ${IMGFILE}.bak
fi
time julia --project  -e 'include("./test/update_packages.jl");'
time julia --project  -e 'include("./test/create_sysimage.jl");'
cp ${IMGFILE} examples/