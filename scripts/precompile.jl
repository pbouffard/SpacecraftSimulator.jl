# using AbstractPlotting
using GLMakie
using MeshIO
using FileIO

s = Scene()
scatter!(s, randn(3000))
display(s)
sleep(1)

s = Scene()
m = mesh!(s, load("../SpacecraftVisualization/assets/new_horizons2.stl"); alpha=0.5, color=(:grey, 0.5)) 
display(s)

s = Scene()
plot(sin.(0:0.01:4π))
display(s)