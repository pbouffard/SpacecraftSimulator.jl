#!/bin/bash
SYSIMAGE_FILE=sysimage.so   
if test -z `ls sysimage.so`
then
  echo $SYSIMAGE_FILE not found
else
  USE_SYSIMAGE="-J $SYSIMAGE_FILE"
fi


julia $USE_SYSIMAGE --project scripts/rebuild_sysimage.jl

REPORT_SYSIMAGE="@info \"Using sysimage: \" PackageCompiler.current_process_sysimage_path()"
export JULIACMDS="using Revise; using PackageCompiler; ${REPORT_SYSIMAGE}"
# echo $JULIACMDS
USE_SYSIMAGE=
echo "USE_SYSIMAGE=$USE_SYSIMAGE"
julia $USE_SYSIMAGE --project -i -e "${JULIACMDS}"
