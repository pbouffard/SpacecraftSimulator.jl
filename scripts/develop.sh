#!/bin/bash
scripts/rebuild_sysimage.jl
USE_SYSIMAGE="-J sysimage.so"
REPORT_SYSIMAGE="@info \"Using sysimage: \" PackageCompiler.current_process_sysimage_path()"
JULIACMDS="using Revise; using PackageCompiler; ${REPORT_SYSIMAGE}"
# echo $JULIACMDS
julia $USE_SYSIMAGE --project -i -e "${JULIACMDS}"