#!/usr/bin/env julia --project
# using GLMakie
# using AbstractPlotting

# To rebuild pre
# julia --project --startup-file=no --trace-compile=precompile_trace.jl scripts/precompile.jl
using PackageCompiler

SCRIPTDIR = dirname(@__FILE__)
MANIFEST_FILE = joinpath(SCRIPTDIR, "..", "Manifest.toml")
MANIFEST_CACHEFILE = "Manifest.toml.cache"
MANIFEST_CACHE = joinpath(SCRIPTDIR, MANIFEST_CACHEFILE)

SYSIMAGEFILENAME = "sysimage.so"
SYSIMAGEBACKUPFILENAME = SYSIMAGEFILENAME * ".backup"

PRECOMPILESTATEMENTSFILE = "scripts/precompile.jl"

MODULES_TO_COMPILE = unique([
  :AbstractPlotting,
  :LinearAlgebra,
  :PrettyPrinting,
  :Infiltrator,
  :StaticArrays,
  :DynamicalSystems,
  :Debugger,
  :GLMakie,
  :AbstractPlotting,
  :StaticArrays,
  :Rotations,
  :Parameters,
])

function call_packagecompiler()
  create_sysimage(
    MODULES_TO_COMPILE; 
    precompile_execution_file=PRECOMPILESTATEMENTSFILE, 
    sysimage_path=SYSIMAGEFILENAME)
end

function build_sysimage()
  try
    @info "Building sysimage including these modules:"
    @info MODULES_TO_COMPILE
    call_packagecompiler()
  catch
    @error "Error creating system image"
    rethrow()
  end

  if isfile(SYSIMAGEFILENAME)
    cp(SYSIMAGEFILENAME, SYSIMAGEBACKUPFILENAME; force=true)
    cp(MANIFEST_FILE, MANIFEST_CACHEFILE; force=true)
  else
    @error "$SYSIMAGEFILENAME not found"
    return false
  end
  
  return true
end

# https://discourse.julialang.org/t/how-to-obtain-the-result-of-a-diff-between-2-files-in-a-loop/23784/4?u=airpmb
function filesidentical(path1::AbstractString, path2::AbstractString)
  stat1, stat2 = stat(path1), stat(path2)
  if !(isfile(stat1) && isfile(stat2)) || filesize(stat1) != filesize(stat2)
      return false # or should it throw if a file doesn't exist?
  end
  stat1 == stat2 && return true # same file
  open(path1, "r") do file1
      open(path2, "r") do file2
          buf1 = Vector{UInt8}(undef, 32768)
          buf2 = similar(buf1)
          while !eof(file1) && !eof(file2)
              n1 = readbytes!(file1, buf1)
              n2 = readbytes!(file2, buf2)
              n1 != n2 && return false
              0 != Base._memcmp(buf1, buf2, n1) && return false
          end
          return eof(file1) == eof(file2)
      end
  end
end

function main()
  manifest_exists = isfile(MANIFEST_FILE)
  if !manifest_exists
    @error "💀 FATAL: Manifest.toml not found"
    error("Exiting")
    exit(-1)
  end

  sysimage_exists = isfile(SYSIMAGEFILENAME)
  manifestcache_exists = isfile(MANIFEST_CACHEFILE)
  if !sysimage_exists || !manifestcache_exists || !filesidentical(MANIFEST_CACHEFILE, MANIFEST_FILE)
    sysimage_exists || (@info "No sysimage found")
    if manifestcache_exists
      @info "Manifest and cache differ"
    else
      @info "No manifest cache file found"
    end
    @info "Building sysimage"
    stats = @timed build_sysimage()
    @info "Elapsed: $(stats.time) seconds"
  else
    @info "Sysimage already exists, and manifest unchanged.. nothing to do 😀"
  end

end

main()


