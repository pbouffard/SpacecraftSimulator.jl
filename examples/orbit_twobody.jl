using SpacecraftSimulator
using SpacecraftSimulator.OrbitTwoBody: keplerian
using SpacecraftSimulator.Constants
using SpacecraftSimulator.Utils: M_to_f
using Dates

function propagate()
  epoch = DateTime(2019, 1, 1, 12, 0, 0)
  
  e = 0.65
  M_deg = 0.0
  oe0 = (;
  
  a_m=R_EARTH_m + 15_000e3, # a, Semi-major axis [m]
  e, # e, Eccentricity [dimensionless]
  i_deg=23.5, # i, Inclination [deg]
  Ω_deg=45.0, # Ω, Right Ascension of the Ascending Node (RAAN) [deg]
  ω_deg=30.0, # ω, Argument of Pe rigee [deg]
  M_deg=0.0, # M, Mean anomaly [deg]
  f_deg=rad2deg(M_to_f(e, deg2rad(M_deg))),
  )
  
  libs = (:SatelliteDynamics, :SatelliteToolbox)
  sols = []
  sols = begin
    sols = []
    for (i, lib) in enumerate(libs)
      @show lib
      push!(sols, keplerian(epoch, oe0; library=lib))
    end
    sols
  end
  return sols
end

# f3d = newfigure3d("foo";axmax=R_EARTH/1e6)
function showfigure(sols)
  SCALE = 1/1e3
  plotopts = (; markersize=0.25, linewidth=2)
  cf = combo_figure(;num_2d_axes=3, axmax=10, resolution=(800,800))
  
  sol_colors = [:blue, :red]
  # 3d stuff
  for (isol, sol) in enumerate(sols)
    rscaled = sol.r*SCALE
    ri = (getindex.(sol.r, iax)*SCALE for iax in 1:3)
    orb_scatter = scatter!(cf.lscene, ri...; show_axis=false, color=sol_colors[isol])
  end
  
  planet_mesh = planet!(cf.lscene, R_EARTH_m*SCALE)
  
  # 2d stuff
  ax_names = ["x", "y", "z"]
  for iax in 1:3
    for (isol, sol) in enumerate(sols)
      eci_scatters = []
      ri = getindex.(sol.r, iax)*SCALE
      ax = cf.axs[iax]
      push!(eci_scatters, scatterlines!(ax, sol.t, ri; color=sol_colors[isol], plotopts...))
      # ylims!(cf.axs[iax], extrema(SCALE*sol[1:3,:][:])...)
    end
    # @show ax.title
  end

  foreach(1:3) do iax
    autolimits!(cf.axs[iax])
    cf.axs[iax].title = "ECI $(ax_names[iax])"
  end
  
  cf.time_slider.range[] = LinRange(0, 1, length(first(sols).t))
  
  # mean_alt = norm.(eachcol(ret.eci[1:3,:]*SCALE)) .- R_EARTH*SCALE
  
  # mean_alt_scatter = scatter!(cf.axs[2], ret.t, mean_alt; plotopts...)
  # ylims!(cf.axs[2], extrema(mean_alt)...)
  
  
  # xlims!(cf.axs[2], extrema(ret.t))
  # cf.axs[2].title = "Mean altitude"
  
  # function lookcenter(scene, stopper)
  #   @async begin
    #     @info "begin async, stopper=$stopper[]"
    #     while !stopper[] && isopen(scene)
    #       # @info "loop"
    #       sleep(0.25)
    #       ep = scene.camera.eyeposition[]
    #       update_cam!(scene, ep, Vec3f0(0))
    #     end
    #     @info "leaving async"
    #   end
    # end
    
    # stopper = Ref(false)
    
    # lookcenter(cf.lscene.scene, stopper)
    
    # return stopper
    #       ep = scene.camera.eyeposition[]
    #       update_cam!(scene, ep, Vec3f0(0))
    # display(cf.fig)
    
  setup_shortcuts(cf.lscene.scene)
  cf.lscene.scene.camera.eyeposition = [30e3, 0, 0]
  point_at_origin(cf.lscene.scene)
  return (; combofig=cf, planet_mesh)
end
  
sols = propagate()


using SpacecraftSimulator.MakieUtils: newfigure3d, newfigure2d, combo_figure, planet!, setup_inspector, setup_shortcuts, point_at_origin, spheremesh
using GLMakie

# function fixfigure(cf)
#   ep = cf.lscene.scene.camera.eyeposition[]
#   update_cam!(cf.lscene.scene, ep, Vec3f0(0))
# end
  
  
  
  resfig = showfigure(sols);
  # point_at_origin(cf.lscene.scene)